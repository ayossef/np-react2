# Adding Redux Support

## Install required packeges

We have two main pakcages to install to add redux support to our react app.
- react-redux 
- @reduxjs/toolkit

Note: we are not installing redux & react-redux
```bash
npm install @reduxjs/toolkit  react-redux
```

## Create Code Elements

### Configure the store
Store: is where your states is living. We configure the store by provding a reducer.

### Create a Reducer and Actions
Reducer is created by providing three params:
- Name
- Initial Value
- Possible update methods (actions)

### Providing the store among certain scope (App)
We use Provider to make the store visible and usable among all sub components. It is done on App scope by default.

### Include store in a component for update
We include the store for update by introucing useDispatch Hook. 

### Include store in a component for reading
We include the store for reading by introducing the useSelector hook.