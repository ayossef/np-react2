# Installing required tools

## Install node and NPM

### Installing node via ubuntu servers

```bash
sudo apt update
sudo apt install nodejs
```
Note: ubuntu pakcages are not the latest 


### Installing node via node server
https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
```bash
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install nodejs
node --version
npm --version
```

## Installing VS Code
Downloading the deb file from VS Code site
```bash
sudo dpkg -i code.deb
```

### Installing the react starter
Installing the react app initializer as global package
```bash
sudo npm install -g create-react-app
```

### Creating our first app
Create first react app
```bash
create-react-app app-name
```

### Run it for the first time
Go into the project directory and start the devlopment server
```bash
cd app-name
npm start
```


