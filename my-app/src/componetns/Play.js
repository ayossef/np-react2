import React, { Component } from 'react'

export default class Play extends Component {
    state = {
        randomValue:0,
        userScore:0
    }
    newGame = ()=>{
        console.log('Game Started')
        let rand = Math.round(Math.random())
        this.setState({
            randomValue: rand
        })
        console.log(this.state.randomValue)
    }
    zeroGuess =() =>{
        if(this.state.randomValue === 0){
            this.setState({
                userScore: this.state.userScore + 1
            })
        }
    }
    oneGuess =() =>{
        if(this.state.randomValue === 1){
            this.setState({
                userScore: this.state.userScore + 1
            })
        }
    }
  render() {

    return (
      <div>
        <h4>Play Here</h4>
        <button onClick={this.newGame}> Start Playing .. </button>
        <br></br>
        <button onClick={this.zeroGuess}> I guess it is Zero </button>
        <button onClick={this.oneGuess}> I guess it is One </button>
        <h4> Your Current Scrore is {this.state.userScore}</h4>
      </div>
    )
  }
}
