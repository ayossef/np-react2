import React from 'react'
import Play from './Play'

function Game() {
  return (
    <div>
        <h3>SQUID Games</h3>
        <img src="https://www.thesquidgame.com/wp-content/uploads/2021/09/The-Squid-Game-Store-x-LOGO.png" alt="Play with us" />
        <Play></Play>    
    </div>
  )
}

export default Game